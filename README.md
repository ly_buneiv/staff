# Readme

Staff module.

## Installation

You can install the package using Composer:

```bash
composer require lybuneiv/staff
```

Then add the service provider to config/app.php:

```bash
Lybuneiv\Staff\StaffServiceProvider::class
```

Publish the config file:

```bash
php artisan vendor:publish --provider="Lybuneiv\Staff\StaffServiceProvider" --tag="config"
```

Then go to config/staff.php and enable migrations=true
Publish the config file:

```bash
php artisan vendor:publish --provider="Lybuneiv\Staff\StaffServiceProvider" --tag="migrations"
```

Then migrate database:

```bash
php artisan migrate
```

Then symlink storage link:

```bash
php artisan storage:link
```

## Usage

For staff route

```bash
/module/staff
```