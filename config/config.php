<?php

// use \lybuneiv\employees\database\seeds\EmployeePositionTableSeeder;

return [
    // automatic loading of routes through main service provider
    'routes' => true,

    /*
    |--------------------------------------------------------------------------
    | Config for views, rendering.
    |--------------------------------------------------------------------------
    */
    'views' => [
        /*
        | The path where the package views are stored.
        | ('{package-path}/resources/views/' by default)
        */
//        'path' => 'resources/views/vendor/employees/'

        'tree' => [
            'lazyChildren' => true,
        ]
    ],

    'publish' => [
        'views' => false,
        'migrations' => false,
        'seeds' => false,
        'factories' => false,
    ],

    /*
    |--------------------------------------------------------------------------
    | Config for seeders.
    |--------------------------------------------------------------------------
    */
];
