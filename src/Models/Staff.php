<?php

namespace Lybuneiv\Staff\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Staff extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'staffs';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'phone', 'gender', 'position', 'photo'];

    /**
     * Returns photo url.
     *
     * @return string
     */
    public function getPhotoUrlAttribute()
    {
        $photo = $this->photo;
        if (!empty($photo)) {
            $photo = Storage::url($photo);
        } else {
            $photo = Storage::url('storage/staff/default_photo.jpg');
	    }

	    return $photo;
    }
}
