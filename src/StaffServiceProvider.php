<?php

namespace Lybuneiv\Staff;

use Illuminate\Support\ServiceProvider;
use Illuminate\Routing\Router;

class StaffServiceProvider extends ServiceProvider
{

	/**
     * Indicates if loading of the provider is deferred.
     *
     * @see http://laravel.com/docs/master/providers#deferred-providers
     * @var bool
     */
    protected $defer = false;

    /**
     * @var string Views path.
     */
    protected $viewsPath = 'resources/views/';

    /**
     * @var array
     */
    protected $commands = [];

    /**
     * Register the service provider.
     *
     * @see http://laravel.com/docs/master/providers#the-register-method
     * @return void
     */
    public function register()
    {
        // $this->commands($this->commands);

        $this->viewsPath = (!empty(config('staff.views.path')))
            ? base_path(config('staff.views.path'))
            : $this->packagePath($this->viewsPath);

        $this->app->make('Lybuneiv\Staff\Controllers\StaffController');
    }

    /**
     * Application is booting
     *
     * @see http://laravel.com/docs/master/providers#the-boot-method
     * @return void
     */
    public function boot()
    {
        $this->publishAll();
        $this->registerViews();
        $this->registerMigrations();
//        $this->registerSeeds();
        // $this->registerFactories();
        $this->registerAssets();
        // $this->registerTranslations();
        $this->registerConfigurations();

        if(! $this->app->routesAreCached() && config('staff.routes')) {
            // dd('s');
            $this->registerRoutes();
        }

        // $this->loadRoutesFrom(__DIR__.'/routes.php');
        // $this->loadViewsFrom(__DIR__.'/resources/views', 'Staff');
    }

    protected function publishAll()
    {
        $this->publishes([
            $this->packagePath('resources/views') => base_path('resources/views/vendor/staff/'),
        ], 'Staff');

        $this->publishes([
                $this->packagePath('database/migrations') => database_path('/migrations')
            ], 'migrations');

        $this->publishes([
            $this->packagePath('resources/assets') => public_path('vendor/lybuneiv/staff'),
        ], 'public');

        $this->publishes([
            $this->packagePath('config/config.php') => config_path('staff.php'),
        ], 'config');
    }
    /**
     * Register the package views
     *
     * @see http://laravel.com/docs/master/packages#views
     * @return void
     */
    protected function registerViews()
    {
        if(!config('staff.publish.views')) {
            $this->loadViewsFrom($this->viewsPath, 'Staff');
        } else {
        	// dd('s');
        	$this->loadViewsFrom(base_path('resources/views/vendor/staff/'), 'Staff');
            $this->publishes([
                $this->packagePath('resources/views') => base_path('resources/views/vendor/staff/'),
            ], 'views');
        }
    }

    /**
     * Register the package migrations
     *
     * @see http://laravel.com/docs/master/packages#publishing-file-groups
     * @return void
     */
    protected function registerMigrations()
    {
        if(!config('staff.publish.migrations')) {
            $this->loadMigrationsFrom($this->packagePath('database/migrations'));
        } else {
            $this->publishes([
                $this->packagePath('database/migrations') => database_path('/migrations')
            ], 'migrations');
        }
    }

    /**
     * Register the package database seeds
     *
     * @return void
     */
    protected function registerSeeds()
    {
        if(!config('staff.publish.seeds')) {
            $this->app->make(Seeder::class)->load($this->packagePath('database/seeds'));
        } else {
            $this->publishes([
                $this->packagePath('database/seeds') => database_path('/seeds')
            ], 'seeds');
        }
    }

    /**
     * Register the package public assets
     *
     * @see http://laravel.com/docs/master/packages#public-assets
     * @return void
     */
    protected function registerAssets()
    {
    	// dd('s');
        $this->publishes([
            $this->packagePath('resources/assets') => public_path('vendor/lybuneiv/staff'),
        ], 'public');
    }

    /**
     * Register the package public assets
     *
     * @see http://laravel.com/docs/master/packages#public-assets
     * @return void
     */
    protected function registerFactories()
    {
        $this->app->make(EloquentFactory::class)->load($this->packagePath('database/factories'));

        if(config('staff.publish.factories')) {
            $this->publishes([
                $this->packagePath('database/factories') => database_path('/factories'),
            ], 'public');
        }
    }

    /**
     * Register the package translations
     *
     * @see http://laravel.com/docs/master/packages#translations
     * @return void
     */
    protected function registerTranslations()
    {
        $this->loadTranslationsFrom($this->packagePath('resources/lang'), 'staff');
    }

    /**
     * Register the package configurations
     *
     * @see http://laravel.com/docs/master/packages#configuration
     * @return void
     */
    protected function registerConfigurations()
    {
        $this->mergeConfigFrom(
            $this->packagePath('config/config.php'), 'staff'
        );
        $this->publishes([
            $this->packagePath('config/config.php') => config_path('staff.php'),
        ], 'config');
    }

    /**
     * Register the package routes
     *
     * @warn consider allowing routes to be disabled
     * @see http://laravel.com/docs/master/routing
     * @see http://laravel.com/docs/master/packages#routing
     * @return void
     */
    protected function registerRoutes()
    {
    	// $this->loadRoutesFrom(__DIR__.'/routes.php');

        $this->app['router']->group([
            // 'namespace' => 'Lybuneiv\Staff\Controllers',
            // 'middleware' => ['web'],
            'prefix' => 'module'
        ], function(Router $router) {
            $router->resource('staff', 'Lybuneiv\Staff\Controllers\StaffController', ['names' => 'staff']);
        });


        // $this->app['router']->group([
        //     'namespace' => __NAMESPACE__,
        //     // 'middleware' => ['web'],
        //     'prefix' => 'admin'
        // ], function(Router $router) {

        	// $this->loadRoutesFrom(__DIR__.'/routes.php');

            // /*
            //  * Other
            //  */
            // $router->get('employee/tree', 'Http\Controllers\EmployeeController@tree')->name('employee.tree');
            // $router->get('employee/{id}/tree/item/children', 'Http\Controllers\EmployeeController@treeItemChildren')->name('employee.tree.item');
            // $router->post('employee/tree/update', 'Http\Controllers\EmployeeController@updateTree')->name('employee.tree.update');


            //  * Web resources

            // $router->resource('employee', 'Http\Controllers\EmployeeController');
            // $router->resource('position', 'Http\Controllers\PositionController');
        	// $router->resource('staff', 'Lybuneiv\Staff\Controllers\StaffController');
            // /*
            //  * API resources
            //  */
            // $router->resource('api/employee', 'Http\Controllers\API\EmployeeController');
            // $router->resource('api/position', 'Http\Controllers\API\PositionController');
        // });
    }

    /**
     * Loads a path relative to the package base directory
     *
     * @param string $path
     * @return string
     */
    protected function packagePath($path = '')
    {
        return sprintf("%s/../%s", __DIR__ , $path);
    }


	/**
	 * Bootstrap the application services.
	 *
	 * @return void
	 */
	// public function boot()
	// {
	// 	$this->loadRoutesFrom(__DIR__.'/routes.php');
	// 	$this->loadViewsFrom(__DIR__.'/resources/views', 'Staff');
	// 	$this->publishes([
	// 		__DIR__.'/routes.php',
	// 		__DIR__.'/resources/views', 'Staff'

	// 	]);

	// }
	/**
	 * Register the application services.
	 *
	 * @return void
	 */
	// public function register()
	// {
	// 	$this->app->make('Lybuneiv\Staff\Controllers\StaffController');
	// }
}