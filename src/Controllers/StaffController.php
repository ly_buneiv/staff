<?php

namespace Lybuneiv\Staff\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;

use Lybuneiv\Staff\Models\Staff;
use Illuminate\Http\Request;

class StaffController extends Controller
{
	/**
	 * @return \Illuminate\Contracts\View\Factory | \Illuminate\View\View
	 */
	public function index(Request $request)
	{
		// dd('sss');
		$limit = $request->limit ?? 1;
		$items = Staff::paginate($limit);
		return view('Staff::index', compact('items'));
	}

	public function create(Request $request)
	{

		$item = new Staff();
		return view('Staff::form', compact('item'));
	}

	public function store(Request $request)
	{

		$data = $this->data($request);

		$result = Staff::create($data);
		if($result){
            return redirect(route("staff.index"))->with(['successful' , __("The staff has been save successfully.")]);
        }

        return redirect(route("staff.index"))->with(['error' , __("Save fails!")]);
	}

	public function edit($id)
	{

		$item = Staff::find($id);
		return view('Staff::form', compact('item'));
	}

	public function update($id, Request $request)
	{

		$item = Staff::find($id);

		$data = $this->data($request);

		$result = $item->update($data);
		if($result){
            return redirect(route("staff.index"))->with(['successful' , __("The staff has been update successfully.")]);
        }

		return redirect(route("staff.index"))->with(['error' , __("Update fails!")]);
	}

	private function data($request = [])
	{
		$requestFile = $request->file('photo');
		// dd($requestFile);
		$data = [
			'name' => $request->name,
			'email' => $request->email,
			'gender' => $request->gender,
			'phone' => $request->phone,
			'position' => $request->position,
			// 'photo' => $request->photo,
		];

		if(!empty($requestFile)) {
			$data['photo'] = $requestFile->store('public/staff/photos');
        }

		return $data;
	}

}