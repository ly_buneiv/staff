<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStaffsTable extends Migration
{

    public $set_schema_table = 'staffs';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->id();
            $table->string('name')->default(null)->nullable();
            $table->string('email')->default(null)->nullable();
            $table->string('phone')->default(null)->nullable();
            $table->string('gender')->default(null)->nullable();
            $table->string('position')->default(null)->nullable();
            $table->string('photo')->default(null)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->set_schema_table);
    }
}
