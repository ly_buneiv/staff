@extends('Staff::master')

@section('staff_content')
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1>Staff Lists</h1>
				</div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="#">Home</a></li>
						<li class="breadcrumb-item">Staff</li>
						<li class="breadcrumb-item active">Lists</li>
					</ol>
				</div>
			</div>
		</div><!-- /.container-fluid -->
	</section>
    <section class="content">
    	<div class="container-fluid">
    		<div class="row">
    			<div class="col-12">
    				@if(session()->has('error'))
					<div class="system-message-container">
					    <div class="alert bg-danger" role="alert">
					        {!! session('error') !!} 
					    </div>
					</div>
					@endif

					@if(session()->has('successful'))
					    <div class="system-message-container">
					        <div class="alert bg-success" role="alert">
					            {!! session('successful') !!} 
					        </div>
					    </div>
					@endif
    			</div>
		        <div class="col-12">
		        	<div class="card">
		              <div class="card-header">
		                <h3 class="card-title">Staff Lists</h3>

		                <div class="card-tools">
		                  	<a href="{{ route('staff.create') }}">Add</a> 
		                </div>
		              </div>
		              <!-- /.card-header -->
		              <div class="card-body table-responsive p-0">
		                <table class="table table-hover">
		                  <thead>
		                    <tr>
		                      <th>No</th>
		                      <th>Name</th>
		                      <th>Email</th>
		                      <th>Phone</th>
		                      <th>Gender</th>
		                      <th>Position</th>
		                      <th>Action</th>
		                    </tr>
		                  </thead>
		                  <tbody>
		                  	@if($items && count($items) > 0)
		                  		@foreach($items as $keyStaff => $staff)
				                    <tr>
				                      <td>{{ $keyStaff }}</td>
				                      <td>{{ $staff->name }}</td>
				                      <td>{{ $staff->email }}</td>
				                      <td>{{ $staff->phone }}</td>
				                      <td>{{ $staff->gender }}</td>
				                      <td>{{ $staff->position }}</td>
				                      <td class="text-center">

                                            <a href="{{ route('staff.edit', $staff->id) }}"><i class="fas fa-pencil-alt" aria-hidden="true"></i>&nbsp;&nbsp;</a>
                                            <a href="javascript:void(0)" class="delete-item" data-url="{{ route('staff.destroy', $staff->id) }}"><i class="far fa-trash-alt"></i></a>

                                      </td>
				                    </tr>
			                    @endforeach
		                    @endif
		                  </tbody>
		                </table>
		              </div>
		              <!-- /.card-body -->
		            </div>
		            <!-- /.card -->
		        </div>
        	</div>
    	</div>
    </section>
@stop