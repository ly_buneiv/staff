@extends('Staff::master')

@section('staff_content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Staff Form</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item">Home</li>
                        <li class="breadcrumb-item"><a href="{{ route('staff.index') }}">Staff</a></li>
                        <li class="breadcrumb-item active">Form</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="card card-primary">
                          <div class="card-header">
                            <h3 class="card-title">Staff Form</h3>
                          </div>
                          <!-- /.card-header -->
                          <!-- form start -->
                          {{ Form::open(["url" => $item->id ? route('staff.update', $item->id) : route("staff.store"), "method" => $item->id ? 'PUT' : 'POST', "id" => "form-role", "enctype" => "multipart/form-data"]) }}
                            <div class="card-body">

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Name</label>
                                                {{ Form::text('name', $item->name ?? '', ['class' => 'form-control']) }}

                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Position</label>
                                                {{ Form::text('position', $item->position ?? '', ['class' => 'form-control']) }}

                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Email</label>
                                                {{ Form::text('email', $item->email ?? '', ['class' => 'form-control']) }}

                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Phone</label>
                                                {{ Form::text('phone', $item->phone ?? '', ['class' => 'form-control']) }}

                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Gender</label>
                                                {{ Form::select('gender', ['Male' => __('Male'), 'Female' => 'Female'], $item->gender ?? 'Male', ['class' => 'form-control']) }}

                                            </div>
                                        </div>

                                    </div>


                                    <div class="form-group">
                                        @if($item && $item->photo)
                                            <img style="width: 100px;" src="{{ $item->photoUrl }}" alt="" class="img-responsive img-thumbnail">
                                        @endif
                                        <label for="exampleInputFile">File input</label>
                                        <div class="input-group">
                                            <div class="custom-file">
                                                <input name="photo" type="file" class="custom-file-input" id="exampleInputFile">
                                                <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                                            </div>

                                        </div>
                                    </div>

                            </div>
                            <!-- /.card-body -->

                            <div class="card-footer">
                              <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                          {{ Form::close() }}
                    </div>
                    <!-- /.card -->
                 </div>
            </div>
        </div>
    </section>


@stop