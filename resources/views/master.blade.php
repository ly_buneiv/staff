<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>

    {{-- Base Meta Tags --}}
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    {{-- Custom Meta Tags --}}
    @yield('meta_tags')

    {{-- Title --}}
    <title>{{ __('Staff') }}</title>

    {{-- Custom stylesheets (pre AdminLTE) --}}
    @yield('adminlte_css_pre')

    {{-- Base Stylesheets --}}

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">


     <link rel="stylesheet" href="{{ asset('vendor/lybuneiv/staff/css/adminlte.min.css') }}">

    {{-- Custom Stylesheets (post AdminLTE) --}}
    @yield('staff_css')

    <link rel="shortcut icon" href="{{ asset('favicons/favicon.ico') }}" />

</head>

<body class="@yield('classes_body')" @yield('body_data')>

    <div class="wrapper">

        <div class="content-wrapper">
            {{-- Body Content --}}
            @yield('staff_content')
        </div>

    </div>
    {{-- Base Scripts --}}
    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" type="text/javascript"></script>

    <script src="{{ asset('vendor/lybuneiv/staff/js/adminlte.min.js') }}" type="text/javascript"></script>


    {{-- Custom Scripts --}}
    @yield('staff_js')

</body>

</html>
